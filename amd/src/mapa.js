// Put this file in path/to/plugin/amd/src
// You can call it anything you like
 
define(['jquery'], function($) {
  var canvas = document.querySelector('canvas');
  var ctx = canvas.getContext('2d');
  
  canvas.width = 900;
  canvas.height = 600;
  var count = 0;
  var fps = 30;
  var percent = 0;
  var direction = 1;
  var lastPath;
  var path = 0;
  var animationComplete = true;
  var buttonClicked;
  var pathPrev;
  var keyPressed;
  var completions = [];
  var assignments = [];
  var rect = {
    x:canvas.width/2 - 85,
    y:canvas.height/2 - 85,
    width:170,
    height:170
  };
  // Imagens
  var spriteLoaded = false;
  var playButton = document.createElement("img");
  playButton.src = './pix/start.png';
  
  var sprite = document.createElement("img");
  sprite.src = './pix/sprite.png';
  sprite.onload = function() {
    spriteLoaded = true;
  };
  
  var keyCodes = [
    {
      key: 40,
      move: 'DOWN'
    },
    {
      key: 38,
      move: 'UP'
    },
    { 
      key: 37,
      move: 'LEFT'
    },
    {
      key: 39,
      move: 'RIGHT'
    }
  ];
  
  var stages =[{
    // 0  - 1 (0)
    start: {
      x: 85,
      y: 90
    },
    end: {
      x: 85,
      y: 315
    },
    type:'line',
    moveForward: 'DOWN',
    color: 'red'
  },{ // 1 - 2 (1)
    start: {
      x: 85,
      y: 315
    },
    end: {
      x: 85,
      y: 540
    },
    type:'line',
    moveForward: 'DOWN',
    moveBackwards: 'UP',
    color: 'red'
  },{ // 2 - 3 (2)
    start: {
      x: 85,
      y: 540
    },
    end: {
      x: 350,
      y: 540
    },
    type:'line',
    moveForward: 'RIGHT',
    moveBackwards: 'UP',
    color: 'red'
  },{ // 3 - 4 (3)
    start: {
      x: 350,
      y: 540
    },
    end: {
      x: 350,
      y: 385
    },
    type:'line',
    moveForward: 'UP',
    moveBackwards: 'LEFT',
    moveForwardTwo: 'RIGHT',
    color: 'red'
  },{ // 4 - 7 (4)
    start: {
      x: 350,
      y: 385
    },
    end: {
      x: 560,
      y: 385
    },
    type:'line',
    moveForward: 'RIGHT',
    moveBackwards: 'DOWN',
    color: 'red'
  },{ // 3 - 5 (5)
    start: {
      x: 350,
      y: 540
    },
    end: {
      x: 560,
      y: 540
    },
    type:'line',
    moveForward: 'RIGHT',
    moveBackwards: 'LEFT',
    color: 'red'
  },{ // 5 - 6 (6)
    start: {
      x: 560,
      y: 540
    },
    end: {
      x: 560,
      y: 385
    },
    type:'line',
    moveForward: 'UP',
    moveBackwards: 'LEFT',
    color: 'red'
  },{ // 6 -7 (7)
    start: {
      x: 560,
      y: 385
    },
    control1: {
      x: 580,
      y: 225
    },
    control2: {
      x: 225,
      y: 400
    },
    end: {
      x: 240,
      y: 220
    },
    type:'cubic',
    moveForward: 'UP',
    moveBackwards: 'DOWN',
    moveBackwardsTwo: 'LEFT',
    color: 'red'
  },{ // 7 -8 (8)
    start: {
      x: 240,
      y: 220
    },
    end: {
      x: 240,
      y: 90
    },
    type:'line',
    moveForward: 'UP',
    moveBackwards: 'DOWN',
    color: 'red'
  },
  { // 8 -9 (9)
    start: {
      x: 240,
      y: 90
    },
    end: {
      x: 583,
      y: 90
    },
    type:'line',
    moveForward: 'RIGHT',
    moveBackwards: 'DOWN',
    color: 'red'
  }, { // 9 (10) só vai pra tras
    start: {
      x: 583,
      y: 90
    },
    end: {
      x: 583,
      y: 90
    },
    type: 'line',
    moveBackwards: 'LEFT',
    color: 'red'
  }];
  
  var sites;
  
  // Map logic
  function animate() {
    if (percent === 100 && direction === 1){ // 0 - 100
      if(path === 4) changePath(2);
      changePath();
      saveVars();
    } else if (percent === 0 && direction === -1){ // 100 - 0
      if(path === 5) changePath(-2, 0, -1);
      saveVars();
    } else {
      draw(percent, path);
      percent += direction;
      // Loop animate
      window.requestAnimationFrame(animate);
    }
  }
  
  window.requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.msRequestAnimationFrame     ||
            function(callback){
              window.setTimeout(callback, 1000 / fps);
            };
  })();
  
  
  // draw the current frame based on sliderValue
  function draw(sliderValue, path) {
  
    // redraw path
    drawPath();
    var xy;
    var percent = sliderValue / 100;
    if (stages[path].type === 'line') {
      xy = getLineXYatPercent(stages[path].start, stages[path].end, percent);
    }
    else if (stages[path].type === 'quadratic') {
      xy = getQuadraticBezierXYatPercent(stages[path].start, stages[path].control, stages[path].end, percent);
    }
    else if (stages[path].type === 'cubic')  {
      xy = getCubicBezierXYatPercent(stages[path].start, stages[path].control1, stages[path].control2, stages[path].end, percent);
    }
    drawSprite(xy);
  }
  
  // draw sprite on xy
  function drawSprite(point) {
    var cellWidth = 100;
    var cellHeight = 150;
    var currentCell = findSpriteCell();
    if (spriteLoaded)
    ctx.drawImage(sprite, currentCell.x * cellWidth, currentCell.y * cellHeight, cellWidth, cellHeight, point.x - 10, point.y - 25, 20, 30);
  }
  
  function findSpriteCell (){
    var currentCell = {x: 0 , y: 0};
    if (percent != 99 && percent != 1){
      currentCell.x = findSpriteCellX();
      currentCell.y = findSpriteCellY();
    }
    return currentCell;
  }
  
  function findSpriteCellY(){
    var index = keyCodes.map(function(obj) { return obj.move; }).indexOf(keyPressed);
    if (path === 7 && 85 > percent && 15 < percent){
      if(keyPressed === 'DOWN') index = 3;
      if(keyPressed === 'UP') index = 2;
    }
    return index != -1 ? index : 0;
  }
  
  function findSpriteCellX(){
    var velocity = 4;
    count++;
    count %= (4 * velocity);
    return Math.floor(count / velocity);
  }
  
  // path: percent is 0-1
  function getLineXYatPercent(startPt, endPt, percent) {
    var dx = endPt.x - startPt.x;
    var dy = endPt.y - startPt.y;
    var X = startPt.x + dx * percent;
    var Y = startPt.y + dy * percent;
    return ({ x: X, y: Y });
  }
  
  // quadratic bezier: percent is 0-1
  function getQuadraticBezierXYatPercent(startPt, controlPt, endPt, percent) {
    var x = Math.pow(1 - percent, 2) * startPt.x + 2 * (1 - percent) * percent * controlPt.x + Math.pow(percent, 2) * endPt.x;
    var y = Math.pow(1 - percent, 2) * startPt.y + 2 * (1 - percent) * percent * controlPt.y + Math.pow(percent, 2) * endPt.y;
    return ({ x: x, y: y });
  }
  
  // cubic bezier percent is 0-1
  function getCubicBezierXYatPercent(startPt, controlPt1, controlPt2, endPt, percent) {
    var x = CubicN(percent, startPt.x, controlPt1.x, controlPt2.x, endPt.x);
    var y = CubicN(percent, startPt.y, controlPt1.y, controlPt2.y, endPt.y);
    return ({ x: x, y: y });
  }
  
  // cubic helper formula at percent distance
  function CubicN(pct, a, b, c, d) {
    var t2 = pct * pct;
    var t3 = t2 * pct;
    return a + (-a * 3 + pct * (3 * a - a * pct)) * pct
      + (3 * b + pct * (-6 * b + b * 3 * pct)) * pct
      + (c * 3 - c * 3 * pct) * t2
      + d * t3;
  }
  
  
  function isInside(pos, rect){
    return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y;
  }
  
  // Draws the path so the user can see
  function drawPath () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = 5;
    for (var i = 0; i < stages.length; i++) { 
      ctx.beginPath();
      ctx.moveTo(stages[i].start.x, stages[i].start.y);
      ctx.strokeStyle = stages[i].color;
      if(stages[i].type === 'line'){
        ctx.lineTo(stages[i].end.x, stages[i].end.y);
      }else if (stages[i].type === 'quadratic'){
        ctx.quadraticCurveTo(stages[i].control.x, stages[i].control.y, stages[i].end.x, stages[i].end.y);
      }else if (stages[i].type === 'cubic'){
        ctx.bezierCurveTo(stages[i].control1.x, stages[i].control1.y, stages[i].control2.x,
          stages[i].control2.y, stages[i].end.x, stages[i].end.y);
      }
      ctx.stroke();
    }
    drawArcs();
  }
  
  function drawArcs() {
    for (var i = 0; i < stages.length; i++) { 
      ctx.beginPath();
      ctx.arc(stages[i].start.x, stages[i].start.y, 10, 0, 2 * Math.PI);
      ctx.moveTo(stages[i].start.x, stages[i].start.y);
      ctx.fillStyle = findColor(i);
      ctx.fill();
    }
  }
  
  function colorPaths(){
    for (var i = 0; i <= lastPath; i++){
      stages[i].color = 'green';
    }
    if(stages[3].color === 'green'){
      stages[5].color = 'green';
    }   
  }
  
  function findColor(stage){
    if(stage === 5) stage -= 2;
    if(stage > 5 && stage < 11) stage--;
    if(assignments[stage] <= completions[stage]) return 'green';
    return 'red';
  }
  
  function changePath(pathChange, progress, directions){
    pathChange = (pathChange === undefined || typeof pathChange !== 'number') ? 1 : pathChange;
    progress = (progress === undefined || typeof progress !== 'number') ? 0 : progress;
    directions = (directions === undefined || typeof directions !== 'number') ? 1 : directions;
    pathPrev = path;
    direction = directions;
    path += pathChange;
    percent = progress;
  }
  
  function enterKeyEvent(){
      var topicNumber;
      path > 5 ? topicNumber = path - 1 : topicNumber = path;
      if(topicNumber === 0){
        alert('Você já está aqui, aperte > para progredir no mapa');
        return false;
      }
      if(sites){
        var site = sites.filter(function(obj) { return obj.section === topicNumber;});
        var activity = completions[topicNumber];
        if(activity === site.length) activity--;
        openPopUpCenter(site[activity].url, 'Tarefa do tópico' + site[activity].section);
      }
  }
  
  function progressForward(key){
    if(key === "RIGHT" && path === 3) changePath(2);
    changePath(0);
    animationComplete = false;
    animate();
  }
  
  function progressBackwards(key){
    if(key === "LEFT" && path === 7) changePath(-2, 100, -1); 
    changePath(-1, 100, -1);
    animationComplete = false;
    animate();
  }
  
  function arrowKeyEvents(index){
  
    keyPressed = keyCodes[index].move;
    if (path <= lastPath){
      if(stages[path].moveForward === keyPressed || stages[path].moveForwardTwo === keyPressed){
        progressForward(keyPressed);
      }
    }
    if(stages[path].moveBackwards === keyPressed || stages[path].moveBackwardsTwo === keyPressed){
      progressBackwards(keyPressed);
    }
  }
  
  function keyEvents(e){
    e.preventDefault();
      if(buttonClicked && animationComplete){
        if (e.keyCode === 13){
          enterKeyEvent();
        }
        var index = keyCodes.map(function(obj) { return obj.key; }).indexOf(e.keyCode);
        if(index >= 0) {
          arrowKeyEvents(index);
        }
     }
  }
  
  function getMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    };
  }
  
  function openPopUpCenter(url, title) {
    var w = window.innerWidth/2;
    var h = window.innerHeight * (3/4);
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
  
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
  
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  
    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
    var timer = setInterval(function () {
      if (newWindow.closed) {
          clearInterval(timer);
          window.location.reload(); // Refresh the parent page
      }
    }, 500);
  }
  
  canvas.addEventListener('click', function(e) {
    var mousePos = getMousePos(canvas, e);
    if (isInside(mousePos,rect) && !buttonClicked) {
        draw(percent, path);
        buttonClicked = true;
    }else{
          console.log('clicked outside rect');
    }	
  }, false);
  
  document.onkeydown = keyEvents;
  
  function saveVars(){
    animationComplete = true;
    if(window.localStorage){
      window.localStorage.path = JSON.stringify(path);
      window.localStorage.pathPrev = JSON.stringify(pathPrev);
    }
  }
  
  function getVars(){
    try {
      path = JSON.parse(window.localStorage.path);
      pathPrev = JSON.parse(window.localStorage.pathPrev);
    } catch (e){
      path = 0;
      pathPrev = 0;
    }
  }
  
  function reset () {
    lastPath = 0;
    percent = 0;
    getVars();
    getLastPath();
    colorPaths();
  }
  
  function getLastPath(){
    for (var i = 0; i < 10; i++) {
        if (assignments[i] === completions[i]) {
          lastPath = i;
        }
    }
    if(lastPath > 4) lastPath++;
  }
  
  function getAssignments(activities){
    return activities.filter(function(obj){ return obj.modulename === "Assignment";});
  }
  
  function getSites(activities){
    sites = getAssignments(activities);
    sites = sites.map(function(obj){return {url: obj.url, section: obj.section};});
    getActivitiesOnTopic(sites);
  }
  
  function getActivitiesOnTopic(sites){
    for(var i = 0; i < 10; i++){
      assignments.push(sites.filter(function (value) {
        return value.section === i;
      }).length);
    }
  }
  
  function getCompletionsOnTopic(sectionsCompleted){
    for( var i = 0; i < 10; i++){
      completions.push(sectionsCompleted.filter(function (value) {
        return value === i;
      }).length);
    }
  }
  
  function start (activities, sectionsCompleted){
    if(sectionsCompleted) {
      getCompletionsOnTopic(sectionsCompleted);
    }
    if(activities){
      getSites(activities);
    }
    reset();
    console.log(assignments);
    console.log(completions);
      // start the animation
    playButton.onload = function () {
      ctx.drawImage(playButton, rect.x, rect.y);
      buttonClicked = false;
    };
    }
     
        return {
            init: start,
        };
    });
  
  