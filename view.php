<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of mapa
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_mapa
 * @copyright  2018 Murilo Baptista
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

// teste
require_once($CFG->libdir.'/completionlib.php');
$CFG->cachejs = false;

// começa aqui
$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // mapa instance ID - it should be named as the first character of the module

if ($id) {
    $cm         = get_coursemodule_from_id('mapa', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moduleinstance  = $DB->get_record('mapa', array('id' => $cm->instance), '*', MUST_EXIST);
} elseif ($n) {
    $moduleinstance  = $DB->get_record('mapa', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $moduleinstance->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('mapa', $moduleinstance->id, $course->id, false, MUST_EXIST);
} else {
    print_error(get_string('missingidandcmid',MOD_MAPA_LANG));
}

$PAGE->set_url('/mod/mapa/view.php', array('id' => $cm->id));
require_login($course, true, $cm);
$modulecontext = context_module::instance($cm->id);

//Diverge logging logic at Moodle 2.7
if($CFG->version<2014051200){
	add_to_log($course->id, 'mapa', 'view', "view.php?id={$cm->id}", $moduleinstance->name, $cm->id);
}else{
	// Trigger module viewed event.
	$event = \mod_mapa\event\course_module_viewed::create(array(
	   'objectid' => $moduleinstance->id,
	   'context' => $modulecontext
	));
	$event->add_record_snapshot('course_modules', $cm);
	$event->add_record_snapshot('course', $course);
	$event->add_record_snapshot('mapa', $moduleinstance);
	$event->trigger();
} 

//if we got this far, we can consider the activity "viewed"
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

//are we a teacher or a student?
$mode= "view";

/// Set up the page header
$PAGE->set_title(format_string($moduleinstance->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);
$PAGE->set_pagelayout('course');

	//Get an admin settings 
	$config = get_config(MOD_MAPA_FRANKY);
  	$someadminsetting = $config->someadminsetting;

	//Get an instance setting
	$someinstancesetting = $moduleinstance->someinstancesetting;


//get our javascript all ready to go
//We can omit $jsmodule, but its nice to have it here, 
//if for example we need to include some funky YUI stuff
// $jsmodule = array(
// 	'name'     => 'mod_mapa',
// 	'fullpath' => '/mod/mapa/module.js',
// 	'requires' => array()
// );
// Testando

$courseid = $PAGE->course->id;
$userid = 1003; // $USER->id;
$submissions = block_completion_progress_student_submissions($courseid, $userid);
$activities = block_completion_progress_get_activities($courseid);
$completion = block_completion_progress_completions($activities, $userid, $course, $submissions);
//here we set up any info we need to pass into javascript

//$tst = new completion_info($course);    
//$cm = new stdClass();
//$cm->id = 320;
//$activitycompletion = $tst->get_data($cm, true, $userid);

$test = Array();
$modinfo = get_fast_modinfo($course);

foreach ($activities as $activity){
	if($completion[$activity['id']] != 0){
		$cms = $modinfo->get_cm($activity['id']);
		if ($cms->uservisible) {
	    // User can access the activity.
	    	$sectionscomplete[] = (int)$cms->sectionnum;
		} else if ($cms->availableinfo) {
	    // User cannot access the activity, but on the course page they will
	    // see a link to it, greyed-out, with information (HTML format) from
	    // $cm->availableinfo about why they can't access it.
		} else {
	    // User cannot access the activity and they will not see it at all.
		}
	}

}

        
// $opts =Array();
// $opts['someinstancesetting'] = $test;

$PAGE->requires->js_call_amd('mod_mapa/mapa', 'init', array($sectionscomplete,$activities));
//this inits the M.mod_mapa thingy, after the page has loaded.
// $PAGE->requires->js_init_call('M.mod_mapa.helper.init', array($opts),false,$jsmodule);

//this loads any external JS libraries we need to call
//$PAGE->requires->js("/mod/mapa/js/somejs.js");
//$PAGE->requires->js(new moodle_url('http://www.somewhere.com/some.js'),true);

//This puts all our display logic into the renderer.php file in this plugin
//theme developers can override classes there, so it makes it customizable for others
//to do it this way.
$renderer = $PAGE->get_renderer('mod_mapa');

//From here we actually display the page.
//this is core renderer stuff


//if we are teacher we see tabs. If student we just see the quiz
if(has_capability('mod/mapa:preview',$modulecontext)){
	echo $renderer->header($moduleinstance, $cm, $mode, null, get_string('view', MOD_MAPA_LANG));
}else{
	echo $renderer->notabsheader();
}

echo $renderer->show_intro($moduleinstance,$cm);

//if we have too many attempts, lets report that.
if($moduleinstance->maxattempts > 0){
	$attempts =  $DB->get_records(MOD_MAPA_USERTABLE,array('userid'=>$USER->id, MOD_MAPA_MODNAME.'id'=>$moduleinstance->id));
	if($attempts && count($attempts)<$moduleinstance->maxattempts){
		echo get_string("exceededattempts",MOD_MAPA_LANG,$moduleinstance->maxattempts);
	}
}

//This is specfic to our renderer
echo $renderer->show_canvas();

// Finish the page
echo $renderer->footer();
